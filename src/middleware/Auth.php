<?php
declare (strict_types = 1);

namespace ric\auth\middleware;

use Closure;
use think\Request;
use think\exception\HttpException;
use think\facade\Event;


/**
 * 登录、Auth初始化
 */
class Auth
{

    /**
     * Auth初始化
     * @access public
     * @param Request $request
     * @param Closure $next
     * @param App     $app
     * @param Session $session
     * @return void
     */
    public function handle($request, Closure $next, $data)
    {
        $data[1] = $data[1]??'';
        //实例化auth
        $website_id = $request->middleware('website_id')??($request->param('base_website_id')??0);
        // print_r($data);exit;
        $this->auth = \ric\auth\Auth::instance($data[0],$website_id);
		$request->auth = $this->auth;

        switch ($data[1]){
            case 'login':
                $account = $request->middleware('account')??$request->param('account',false);
                $password = $request->middleware('password')??$request->param('password',false);
                if(!$account || !$password){
                    return tofalse(105);
                }
                $returndata = $this->auth->getLogin($account,$password);
                if($returndata){
                    $this->setlog('登录成功');
                    return totrue($returndata,'登录成功');
                }else{
                    return tofalse(-1,'账号密码错误');
                }
                break;
            case 'logout':
                $returndata = $this->auth->getLogout();
                $this->setlog('退出登录成功');
                return totrue($returndata,'退出登录成功');
                break;
            case 'getinfo':
                $returndata = $this->auth->getUserInfo();
                if(!$returndata){
					if($this->auth->getConfig()['url'] == ''){
						return tofalse(106);
					}else{
						return redirect($this->auth->getConfig()['url']);
					}
                    
                }
                return totrue($returndata,'获取登录信息成功');
                break;
            case 'getrule':
                $returndata = $this->auth->getRule();
                if(!$returndata){
                    if($this->auth->getConfig()['url'] == ''){
                    	return tofalse(106);
                    }else{
                    	return redirect($this->auth->getConfig()['url']);
                    }
                }
                return totrue($returndata,'获取权限列表成功');
                break;
        }
        
        //是否需要判断是否已经登录
        if(!isset($data[0]) || !session('login.'.$data[0].$this->auth->website_id)){
            if($this->auth->getConfig()['url'] == ''){
            	return tofalse(106);
            }else{
            	return redirect($this->auth->getConfig()['url']);
            }
        }

        //是否判断权限
        $controller = request()->controller();
        $action = request()->action();

        if(isset($data[1]) && in_array($data[1],['auth','log'])){
            if(!$this->auth->check($controller . '-' . $action, session('login.'.$data[0].$this->auth->website_id))){
                return tofalse(101);
            }
        }

        
        $response = $next($request);
        if(isset($data[1]) && in_array($data[1],['log','login','logout'])) {
            $getData = $response->getData();
            $message = $getData['message']??'';
            $this->setlog($message);
        }
        return $response;
    }

    private function setlog($message){
        //是否记录日志
//        if(isset($data[1]) && in_array($data[1],['log','login','logout'])) {
//            Event::listen('HttpEnd', function ($response)use($this->auth) {
//                $getData = $response->getData();
//                $message = $getData['message']??'';
//            });
//        }
        $this->auth->setLog($message);
    }
}
